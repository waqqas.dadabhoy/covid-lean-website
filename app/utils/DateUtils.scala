package utils

import java.time.LocalDate

object DateUtils {
  def betweenInclsive(date: LocalDate, startIncl: LocalDate, endIncl: LocalDate): Boolean = {
    (date.isAfter(startIncl) || date.isEqual(startIncl)) && (date.isBefore(endIncl) || date.isEqual(endIncl))
  }
}

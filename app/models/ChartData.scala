package models

import play.api.libs.json.Json

case class ChartDataPoint(x: String, y: Long) {

}

case class ChartDataset(label: String, data: Seq[ChartDataPoint], backgroundColor: String, borderColor: String, fill: Boolean = false, borderWidth: Int = 1, pointRadius: Int = 1) {

}

case class ChartData(labels: Seq[String], datasets: Seq[ChartDataset]) {

  def asJson: String = Json.stringify(Json.toJson(this)(ChartData.chartDataFormat))
}

object ChartData {
  implicit val chartDataPointFormat = Json.format[ChartDataPoint]
  implicit val chartDatasetFormat = Json.format[ChartDataset]
  implicit val chartDataFormat = Json.format[ChartData]
}

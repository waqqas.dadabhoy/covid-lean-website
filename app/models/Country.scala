package models

case class Country(name: String) extends AnyVal {
  override def toString: String = name
}

package models

import utils.DateUtils

import java.time.LocalDate
import scala.collection.SortedSet
import scala.util.Try

case class Dataset(countryData: Map[Country, CountryDataset]) {
  def apply(country: Country) = countryData.apply(country)
}

object Dataset {
  def fromCsvs(confirmed: Map[Country, Map[LocalDate, Long]], recovered: Map[Country, Map[LocalDate, Long]], deaths: Map[Country, Map[LocalDate, Long]]): Try[Dataset] = Try {
    val countries = confirmed.keySet intersect recovered.keySet intersect deaths.keySet
    val dataset = countries.map { country =>
      (country, CountryDataset(confirmed(country), recovered(country), deaths(country)))
    }.toMap
    Dataset(dataset)
  }
}

case class CountryDataset(confirmedCumulative: Map[LocalDate, Long], recoveredCumulative: Map[LocalDate, Long], deathsCumulative: Map[LocalDate, Long]) {

  def availableDates(startIncl: LocalDate = LocalDate.parse("2000-01-01"), endIncl: LocalDate = LocalDate.parse("9999-12-21")): SortedSet[LocalDate] = {
    val intersection = confirmedCumulative.keySet.intersect(recoveredCumulative.keySet).intersect(deathsCumulative.keySet)
    val withinRange = intersection.filter(DateUtils.betweenInclsive(_, startIncl, endIncl))

    require(withinRange.size > 2)
    SortedSet.from(withinRange)
  }


  val latestDate: LocalDate = availableDates().max
  val secondLatestDate: LocalDate = availableDates().maxBefore(latestDate).get

  def apply(date: LocalDate): Option[DailyData] = if (availableDates().contains(date)) {
    Some(DailyData(confirmedCumulative(date), recoveredCumulative(date), deathsCumulative(date)))
  } else None

  val latestData: DailyData = {
    DailyData(confirmedCumulative(latestDate), recoveredCumulative(latestDate), deathsCumulative(latestDate))
  }

  val latestChange: DailyData = {
    val secondLatestData = DailyData(confirmedCumulative(secondLatestDate), recoveredCumulative(secondLatestDate), deathsCumulative(secondLatestDate))
    DailyData(latestData.confirmed-secondLatestData.confirmed, latestData.recovered-secondLatestData.recovered, latestData.deaths-secondLatestData.deaths)
  }

  def dailyChanges(startIncl: LocalDate = LocalDate.parse("2000-01-01"), endIncl: LocalDate = LocalDate.parse("9999-12-21")): Seq[(LocalDate, DailyData)] = {
    availableDates(startIncl, endIncl).tail.toSeq.map { date: LocalDate =>
      val thisDateData = apply(date).get
      val previousDate = availableDates().maxBefore(date)
      val previousDateData = apply(previousDate.get).get

      (date, thisDateData-previousDateData)
    }
  }
}

case class DailyData(confirmed: Long, recovered: Long, deaths: Long) {
  val activeCases = confirmed-recovered-deaths

  def -(that: DailyData): DailyData = DailyData(this.confirmed-that.confirmed, this.recovered-that.recovered, this.deaths-that.deaths)
}

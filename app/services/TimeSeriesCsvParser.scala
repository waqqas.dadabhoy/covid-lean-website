package services

import models.Country
import org.apache.commons.csv.{CSVFormat, CSVParser}

import java.io.InputStream
import java.net.URL
import java.nio.charset.StandardCharsets
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import scala.jdk.CollectionConverters._
import scala.util.Try

object TimeSeriesCsvParser {
  val dateFormat = DateTimeFormatter.ofPattern("M/d/yy")

  def parse(url: URL): Try[Map[Country, Map[LocalDate, Long]]] = {
    scala.util.Using(CSVParser.parse(url, StandardCharsets.UTF_8, CSVFormat.DEFAULT.withFirstRecordAsHeader())){ rows =>
      parse(rows)
    }
  }

  def parse(in: InputStream): Try[Map[Country, Map[LocalDate, Long]]] = {
    scala.util.Using(CSVParser.parse(in, StandardCharsets.UTF_8, CSVFormat.DEFAULT)){ rows =>
      parse(rows)
    }
  }

  def parse(rows: CSVParser): Map[Country, Map[LocalDate, Long]] = {
    val dates = getDatesInCsv(rows)

    rows.asScala.map { row =>
      val countryName = row.get(1)

      val dataPoints = (4 until row.size()).map { colNum =>
        val date = dates(colNum)
        val value = row.get(colNum)
        (date, value.toLong)
      }.toMap

      (Country(countryName), dataPoints)
    }.toMap
  }

  def getDatesInCsv(rows: CSVParser): Map[Int, LocalDate] = {
    val headerNames = rows.getHeaderNames.asScala
    (4 until headerNames.size).map { colNum =>
      val headerName = headerNames(colNum)
      (colNum, LocalDate.parse(headerName, dateFormat))
    }.toMap
  }
}

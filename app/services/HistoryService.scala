package services

import models.{Country, Dataset}

import java.net.URL
import java.time.{Instant, LocalDate, ZoneId, ZonedDateTime}
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration
import scala.jdk.DurationConverters._
import scala.util.{Failure, Success, Try}

object HistoryService {
  val cacheDuration = Duration(1, TimeUnit.DAYS)

  private val confirmedUrl = new URL("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv")
  private val recoveredUrl = new URL("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv")
  private val deathsUrl = new URL("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv")

  def today = LocalDate.now()
  def yesterday = today.minusDays(1)

  private var lastUpdated: ZonedDateTime = Instant.EPOCH.atZone(ZoneId.systemDefault())
  private var cachedData: Dataset = _

  getData(false).get // prevent instantiating class if initial request doesn't work

  def getData(allowStale: Boolean = true): Try[Dataset] = this.synchronized {
    if (ZonedDateTime.now().isAfter(lastUpdated.plus(cacheDuration.toJava))) {
      val confirmed = TimeSeriesCsvParser.parse(confirmedUrl)
      val recovered = TimeSeriesCsvParser.parse(recoveredUrl)
      val deaths = TimeSeriesCsvParser.parse(deathsUrl)

      val result = confirmed.flatMap { c =>
        recovered.flatMap { r =>
          deaths.flatMap { d =>
            Dataset.fromCsvs(c, r, d)
          }
        }
      }

     result match {
       case Success(value) =>
         this.cachedData = value
         this.lastUpdated = ZonedDateTime.now()
         result
       case Failure(exception) =>
         if (allowStale) Success(this.cachedData) else result
     }

    } else Success(cachedData)
  }
}

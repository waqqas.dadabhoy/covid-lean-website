package services

import play.api.libs.json.{JsError, JsSuccess, Json}

import java.net.URI
import java.net.http.HttpResponse.BodyHandlers
import java.net.http.{HttpClient, HttpRequest}
import java.time.{Instant, ZoneId, ZonedDateTime}
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.jdk.DurationConverters._

object StatsFetcher {
  case class UpstreamFormat(testedPositive: Long, recovered: Long, activeCases: Long, inICU: Long, respiratoryAid: Long, deceased: Long, country: String)

  private val url = URI.create("https://api.apify.com/v2/key-value-stores/6t65lJVfs3d8s6aKc/records/LATEST?disableRedirect=true")
  private val cacheDuration: FiniteDuration = Duration(1, TimeUnit.DAYS)
  private val httpClient = HttpClient.newBuilder().build()
  private implicit val upstreamFormatReads = Json.reads[UpstreamFormat]

  private var latestData: UpstreamFormat = _
  private var lastUpdated: ZonedDateTime = Instant.EPOCH.atZone(ZoneId.systemDefault())

  def getData(): UpstreamFormat = synchronized {
    if (ZonedDateTime.now().isAfter(lastUpdated.plus(cacheDuration.toJava))) {
      val request = HttpRequest.newBuilder(url).build()
      val response = httpClient.send(request, BodyHandlers.ofString())
      val responseBody = response.body()
      val parsed = Json.parse(responseBody).validate[UpstreamFormat]
      parsed match {
        case JsSuccess(value, path) =>
          this.latestData = value
          this.lastUpdated = ZonedDateTime.now()
          value
        case JsError(errors) => latestData // TODO handle case where first call to API returns error
      }
    } else latestData
  }
}

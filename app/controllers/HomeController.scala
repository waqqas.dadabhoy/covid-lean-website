package controllers

import models._
import play.api.mvc._
import services.{HistoryService, StatsFetcher}

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.inject._
import scala.util.{Failure, Success, Try}

@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {
  val dateFormat = DateTimeFormatter.ISO_DATE

  def index(chartStart: String, chartEnd: String) = Action { implicit request: Request[AnyContent] =>
    val chartStartDate = Try(LocalDate.parse(chartStart, DateTimeFormatter.ISO_LOCAL_DATE)).getOrElse(LocalDate.of(2020, 1, 22))
    val chartEndDate = Try(LocalDate.parse(chartEnd, DateTimeFormatter.ISO_LOCAL_DATE)).getOrElse(LocalDate.now())

    val stats = StatsFetcher.getData()
    val history = HistoryService.getData()

    history match {
      case Success(historyData) =>
        val countryData = historyData(Country("Malaysia"))
        val latestChange = countryData.latestChange
        val (cumulativeChartData, dailyChartData) = generateCharts(countryData, chartStartDate, chartEndDate)
        Ok(views.html.index(stats, latestChange, cumulativeChartData, dailyChartData, chartStartDate, chartEndDate))

      case Failure(exception) => BadRequest(exception.getMessage)
    }
  }

  private def generateCharts(countryData: CountryDataset, start: LocalDate, end: LocalDate): (ChartData, ChartData) = {
    val dateLabels = countryData.availableDates(start, end).toSeq.map(_.toString)

    val chartSeries = Seq(
      ("Confirmed", (x: DailyData) => x.confirmed, "red"),
      ("Recovered", (x: DailyData) => x.recovered, "green"),
      ("Deaths", (x: DailyData) => x.deaths, "black"),
      ("Active Cases", (x: DailyData) => x.activeCases, "orange"),
    )
    val cumulativeChartData = {
      val chartDatasets = chartSeries.map { case (label, extractionFunc, color) =>
        val points = countryData.availableDates(start, end).toSeq.map(d => ChartDataPoint(d.toString, extractionFunc(countryData(d).get)))
        ChartDataset(label, points, color, color)
      }

      ChartData(dateLabels, chartDatasets)
    }

    val dailyChartData = {
      val chartDatasets = chartSeries.map { case (label, extractionFunc, color) =>
        val points = countryData.dailyChanges(start, end).map { case (date, data) => ChartDataPoint(date.toString, extractionFunc(data)) }
        ChartDataset(label, points, color, color)
      }

      ChartData(dateLabels, chartDatasets)
    }

    (cumulativeChartData, dailyChartData)
  }
}

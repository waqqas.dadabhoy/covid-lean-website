lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    name := """covid-lean-website""",
    organization := "",
    version := "1.0-SNAPSHOT",
    scalaVersion := "2.13.5",
    libraryDependencies ++= Seq(
      guice,
      "com.typesafe.play" %% "play-json" % "2.9.2",
      "org.apache.commons" % "commons-csv" % "1.8",
    ),
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings"
    ),
  )
